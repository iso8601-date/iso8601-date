;;;; -*- mode: lisp; package: common-lisp -*-

;;;; ISO8601 date parsing and formatting

;;;; Commentary:

;;;; Code:

(defpackage #:iso8601-date-system
  (:use #:common-lisp
        #:asdf))

(in-package #:iso8601-date-system)

(defsystem #:iso8601-date
  :author "Thomas Russ <tar@isi.edu>"
  :maintainer "Matthew Kennedy <mkennedy@gentoo.org>"
  :components ((:file "packages")
               (:file "iso8601" :depends-on ("packages"))))

;;;; iso8601-date.asd ends here
